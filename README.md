# About me

I'm a simple responsive website for a fictional restaurant with Bootstrap and the modification of Bootstrap with SASS.
Made by Mikail Yagci.

**Used in this Project:**
- HTML5, CSS3, Bootstrap5, SASS/SCSS

# How to work with me

- Install NodeJS & NPM
- Open Project in terminal and run `npm install` and `npm run compile`

For development, run in terminal `npm run compile:watch`

![Landing-Page](images/LandingPage-Akira-Sushi-Restaurant.png)